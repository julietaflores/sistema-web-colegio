<?php
include('../connection.php');
include('../models/model_notas.php');
$notasModel = new Notas_Model();

if( !$notasModel->delete( $_POST['id'] ) ){
    echo "No se pudo eliminar esta nota, porque podría tener registros relacionados.";
}else{
    echo "Se elimino correctamente.";
}
    