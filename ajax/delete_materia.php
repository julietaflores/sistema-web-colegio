<?php
include('../connection.php');
include('../models/model_materia.php');
$materiaModel = new Materia_Model();

if( !$materiaModel->delete( $_POST['id'] ) ){
    echo "No se pudo eliminar la materia, porque podría tener registros relacionados.";
}else{
    echo "Se elimino correctamente.";
}
    