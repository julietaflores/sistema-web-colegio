<style>
    .main_loader{
        background-color: #0c0c0c;
        position: fixed;
        z-index: 10000 !important;
        opacity: 0.6;

        top: 0;
        left: 0;
        min-width: 100%;
        min-height: 100%;
    }

    .loader {
        position: absolute;
        left: 50%;
        top: 50%;
        margin: -75px 0 0 -75px;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #dd4b39;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>

<div class="main_loader" style="display: none">
    <div>
        <div class="loader"></div>
    </div>
</div>

<aside class="main-sidebar">
    <section class="sidebar">

        <div class="user-panel" >
            <div class="pull-left image">
                <img src="<?php echo LOCALHOST;?>/img/icons/user-icon.png" class="img-circle" alt="User Image" style="border-radius: 50%;
                            height:45px;
                           
                            border: 2px solid;
                            border-color: #46595D;
                           
                          ">
            </div>
            <div class="pull-left info">
                <p><?php echo $_SESSION['name_user'];?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Conectado</a>
            </div>
        </div>

        <ul class="sidebar-menu">
            <li class="header" style="color: #ffffff">MENU</li>

            <?php if( $_SESSION['type_user'] == 0){ ?>

                <li>
                <a href="<?php echo LOCALHOST;?>/views/lista_admin.php">
                    <i class="fa fa-circle-o"></i> <span> INICIO</span>
                </a>
            </li>

                <li>   
              <a data-toggle="collapse" href="#lista" class="collapsed" aria-expanded="false">
              <i class="fa fa-circle-o"></i>
                      <span >GESTIONAR USUARIOS
                         <b class="caret"></b>
                      </span>
              </a>

              <div class="collapse" id="lista" aria-expanded="false" style="height: auto;">
                      <ul class="nav">
                          <li><a href="<?php echo LOCALHOST;?>/views/lista_administrador.php" > <span class="badge badge-success"></span>  Administrador </a></li>
                          <li><a href="<?php echo LOCALHOST;?>/views/lista_alumno.php"><span class="badge badge-success"></span>Alumno</a></li>
                          <li><a href="<?php echo LOCALHOST;?>/views/lista_docente.php"><span class="badge badge-success"></span>Profesor</a></li>
                      </ul>
              </div>
          </li>

                <li>
                <a href="<?php echo LOCALHOST;?>/views/lista_nivel.php">
                    <i class="fa fa-circle-o"></i> <span> NIVELES</span>
                </a>
            </li>


        

           

            <li>
                <a href="<?php echo LOCALHOST;?>/views/lista_curso.php">
                    <i class="fa fa-circle-o"></i> <span> CURSO</span>
                </a>
            </li>

            <li>
                <a href="<?php echo LOCALHOST;?>/views/lista_materia.php">
                    <i class="fa fa-circle-o"></i> <span> MATERIA</span>
                </a>
            </li>


            <li>
                <a href="<?php echo LOCALHOST;?>/views/lista_oferta.php">
                    <i class="fa fa-circle-o"></i> <span> OFERTA</span>
                </a>
            </li>

            <li>
                <a href="<?php echo LOCALHOST;?>/views/lista_inscripcion.php">
                    <i class="fa fa-circle-o"></i> <span> INSCRIPCION</span>
                </a>
            </li>
            <li>
                <a href="<?php echo LOCALHOST;?>/views/lista_notas.php">
                    <i class="fa fa-circle-o"></i> <span> NOTAS</span>
                </a>
            </li>

            <li>
                <a href="<?php echo LOCALHOST;?>/views/lista_reporte.php">
                    <i class="fa fa-circle-o"></i> <span> REPORTE</span>
                </a>
            </li>

            <?php } ?>



            <?php if( $_SESSION['type_user'] == 1){ ?>
           
                <li>
                <a href="<?php echo LOCALHOST;?>/views/list_alumno.php">
                    <i class="fa fa-circle-o"></i> <span> INICIO</span>
                </a>
            </li>


            <li>
                <a href="<?php echo LOCALHOST;?>/views/aoferta.php">
                    <i class="fa fa-circle-o"></i> <span> OFERTA</span>
                </a>
            </li>


            <li>
                <a href="<?php echo LOCALHOST;?>/views/ainscripcion.php">
                    <i class="fa fa-circle-o"></i> <span> INSCRIPCION</span>
                </a>
            </li>

            <li>
                <a href="<?php echo LOCALHOST;?>/views/anota.php">
                    <i class="fa fa-circle-o"></i> <span> NOTA</span>
                </a>
            </li>

            <?php } ?>



            <li>
                <a href="<?php echo LOCALHOST;?>/close_session.php">
                    <span> SALIR</span>
                </a>
            </li>

<br>

            <div class="form-group" >   
                <div class="col-sm-12">
                      <!-- rojo -->
                      <input id="rojo" style="display: none;"  name="coloresc" class="side" type="radio" onclick="marketer()" value="FF2968">
                      <label class="labelcolo" for="rojo" style="background: #FF2968; width: 20px;height: 20px;border-radius:15px;"></label>
 
                      <input  id="lila" style="display: none;" name="coloresc" type="radio" class="side" onclick="marketer()" value="CC73E1">
                      <label class="labelcolo" for="lila" style="background: #CC73E1 ; width: 20px;height: 20px;border-radius:15px;"></label>
                      <!-- cafe -->
                      <input  id="cafe" style="display: none;" name="coloresc" type="radio" class="side" onclick="marketer()" value="AC8E68">
                      <label class="labelcolo" for="cafe" style="background: #AC8E68 ; width: 20px;height: 20px;border-radius:15px;"></label>
                      <!-- verde -->
                      <input  id="verde" style="display: none;" name="coloresc" type="radio" class="side" onclick="marketer()" value="32D74B">
                      <label class="labelcolo" for="verde" style="background: #32D74B ; width: 20px;height: 20px;border-radius:15px;"></label>
   
      
                    </div>                                 
                </div>
                <input type="hidden" class="color" id="color" name="color" value="<?php echo $color['color']; ?>"/>
              

        </ul>
    </section>
</aside>
<form id="form_search1" method="post" style="display: none;">
    <input name="color" id="search_text_form1">
</form>


<script type="text/javascript">	
  $(document).ready(function() {
    var colores = $(".color").val();

    var header = $(".navbar");
    var sidebar = $(".user-header");
    var gg= $(".sidebar-toggle");

    header.css({"background-color":colores});
    sidebar.css({"background-color":colores});
    gg.css({"background-color":colores});
    });
 
function marketer(){
  
    var header = $(".navbar");
    var sidebar = $(".user-header");
    var gg= $(".sidebar-toggle");
    color=$("input[name='coloresc']:checked").val();
    var colores='#'+$("input[name='coloresc']:checked").val();
    header.css({"background-color":colores});
    sidebar.css({"background-color":colores});
    gg.css({"background-color":colores});

    $('#search_text_form1').val(colores);
    $('#form_search1').submit();

}
</script>