<?php
session_start();

$errorMessage = '';
if( isset($_POST['password']) && isset($_POST['password_confirm']) )
{
    if(!empty($_POST['password']) && !empty($_POST['password_confirm'])) {
        if( trim($_POST['password']) == trim($_POST['password_confirm']) )
        {
            include("connection.php");
            include("models/model_user.php");
            $userModel = new administrador_Model();
            $user = $userModel->updatePassword($_SESSION['FORWARD_id_user'], $_POST['password']);
            header("Location: index.php");
            die();
        }
        else
            $errorMessage = '- La Contraseña y la Confirmación de la Contraseña deben de ser iguales.';
    }
    else
        $errorMessage = '- Ningun campo debe de estar vacio.';
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Configuración de contraseña</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="javascript:void(0)">Configuración de contraseña</a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Antes de ingresar por 1ra vez al sistema, por favor configure una contraseña</p>

        <form action="set_password.php" method="post">
            <div class="form-group has-feedback">
                <label>Nombre: <?php echo $_SESSION['FORWARD_name_user'];?></label>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Contraseña"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password_confirm" placeholder="Confirmación de contraseña"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <a href="login.php" class="btn btn-danger btn-block btn-flat">Salir</a>
                </div>
                <div class="col-xs-4">
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">GUARDAR</button>
                </div>
            </div>
            <?php if(!empty($errorMessage) ){ ?>
                <div>
                    <span style="color: red"><b><?php echo $errorMessage; ?></b></span>
                </div>
            <?php }//End if ?>
        </form>
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery 2.1.3 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>