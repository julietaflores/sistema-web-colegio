<?php
include_once('configs.php');
session_start();
include('connection.php');
include("models/model_user.php");

$showError = false;
if( isset($_POST['user']) && isset($_POST['password']) )
{
    $userModel = new administrador_Model();
    $user = $userModel->getValidatedByLoginByPassword($_POST['user'], $_POST['password']);
    
    if( $user == null )
        $showError = true;
    else {
       
         if(isset($user[0]['adm_login'])){
           // print_r("existe");
           // exit();
            $_SESSION['id_user'] = $user[0]['id_admin']; 
            $_SESSION['name_user'] = $user[0]['adm_login']; 
            $_SESSION['type_user'] = $user[0]['adm_padre'];
         }else{
          //  print_r("no  existe");
           // exit();
           $_SESSION['id_user'] = $user[0]['id_alumno']; 
            $_SESSION['name_user'] = $user[0]['nombre']; 
            $_SESSION['type_user'] = $user[0]['type_user'];
           

         }

      
         

        if( empty($_POST['password']) ){
            header('Location: set_password.php');
        }else{   
            header("Location: index.php");
        }
        
        die();
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo WEB_TITLE;?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
</head>
<body class="login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="javascript:void(0)">Autentificación</a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Ingrese su usuario y contraseña</p>

        <form action="login.php" method="post">
            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="user" placeholder="Usuario"/>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Contraseña"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">INGRESAR</button>
                </div>
            </div>
            <?php if ($showError) { ?>
                <div>
                    <span style="color: red"><b>El usuario y/o contraseña son incorrectos!!</b></span>
                </div>
            <?php }//End if ?>
        </form>
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery 2.1.3 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>