<?php

class Inscripcion_Model
{
    public function getEmpty()
    {
        $inscripcion = array(
            'idinscripcion' => '',
            'fecha' => '',
            'hora' => '',
            'idalumno' => '',
            'idmateria' => '',
            'alumnon' => '',
            'materian' => '',
            'docenten' => '',
            'curson' => ''
        );
        return $inscripcion;
    }



    public function getById($id)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM inscripcion
             WHERE idinscripcion = $id ";
      $result = $con->execute_query($query);
      if( pg_num_rows($result) > 0){    
          $combined=array();
          while ($row = pg_fetch_assoc($result)) {
              $combined[]=$row;
          }
         return $combined;
        }else{
            return null;
        }

    }


    public function getByIda($id)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM inscripcion
             WHERE idalumno = $id ";
      $result = $con->execute_query($query);
      if( pg_num_rows($result) > 0){    
          $combined=array();
          while ($row = pg_fetch_assoc($result)) {
              $combined[]=$row;
          }
         return $combined;
        }else{
            return null;
        }

    }




    public function searchByName($name, $limit=9999, $offset=0)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM  inscripcion
             WHERE grado LIKE '%$name%'
             ORDER BY grado
             LIMIT $limit
             OFFSET $offset";
 $result = $con->execute_query($query);
 if( pg_num_rows($result) > 0){    
     $combined=array();
     while ($row = pg_fetch_assoc($result)) {
         $combined[]=$row;
     }
    return $combined;
   }else{
       return null;
   }



    }



    public function getAll($limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM inscripcion
             ORDER BY idinscripcion asc
             LIMIT $limit
             OFFSET $offset";
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }
    }

    public function getAllnn($limit=9999, $offset=0)
    {
        $query =
            "SELECT *
            FROM inscripcion i
           WHERE NOT EXISTS (SELECT NULL
                               FROM nota n
                              WHERE n.idinscripcion = i.idinscripcion )
             LIMIT $limit
             OFFSET $offset";
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }
    }


    public function getByNameExceptId($name, $exceptId='')
    {
        $con = new Connection();
        $where = '';
        if( !empty($exceptId) )
            $where = " AND idinscripcion <> $exceptId ";
        $query =
            "SELECT *
             FROM inscripcion
             WHERE grado = '$name' 
             $where ";

$result = $con->execute_query($query);
if( pg_num_rows($result) > 0){    
    $combined=array();
    while ($row = pg_fetch_assoc($result)) {
        $combined[]=$row;
    }
   return $combined;
  }else{
      return null;
  }

    }



    public function search($search, $limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM inscripcion
             WHERE alumnon LIKE '%$search%' 
             ORDER BY alumnon
             LIMIT $limit
             OFFSET $offset";
        
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }


    }



    public function save($fecha,$hora,$idalumno,$idoferta,$alumnon,$materian,$docenten,$curson)
    {
        $con = new Connection();
        $query =
            "INSERT INTO inscripcion (fecha,hora,idalumno,idoferta,alumnon,materian,docenten,curson) VALUES ('$fecha','$hora',$idalumno,$idoferta,'$alumnon','$materian','$docenten','$curson') RETURNING idinscripcion;";
            $result = $con->getLastInsertedID($query);
            return $result;
    }



    public function update($id,$fecha,$hora,$idalumno,$idoferta,$alumnon,$materian,$docenten,$curson)
    {
        $con = new Connection();
        $query =
            "UPDATE inscripcion
             SET fecha  = '$fecha', hora = '$hora', idalumno=$idalumno,idoferta = $idoferta ,alumnon='$alumnon',materian='$materian' ,docenten='$docenten' , curson='$curson'  WHERE idinscripcion = $id";
        return $con->execute_query($query);
    }



    public function delete($id)
    {
        $con = new Connection();
        $query = "DELETE FROM inscripcion WHERE idinscripcion = $id ";
        return $con->execute_query($query);
    }



}