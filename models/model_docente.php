<?php
class Docente_Model
{
    public function getEmpty()
    {
        $docente = array(
            'iddocente' => '',
            'nombre' => '',
            'apellido' => '',
            'ci' => '',
            'sexo' => '',
            'telefono' => '',
            'correo' => '',
            'direccion' => '',
            'estado' => '',
            'especialidad' => ''
        );
        return $docente;
    }



    public function getById($id)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM docente
             WHERE iddocente = $id ";
      $result = $con->execute_query($query);
      if( pg_num_rows($result) > 0){    
          $combined=array();
          while ($row = pg_fetch_assoc($result)) {
              $combined[]=$row;
          }
         return $combined;
        }else{
            return null;
        }

    }





    public function searchByName($name, $limit=9999, $offset=0)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM docente
             WHERE nombre LIKE '%$name%'
             ORDER BY nombre
             LIMIT $limit
             OFFSET $offset";
 $result = $con->execute_query($query);
 if( pg_num_rows($result) > 0){    
     $combined=array();
     while ($row = pg_fetch_assoc($result)) {
         $combined[]=$row;
     }
    return $combined;
   }else{
       return null;
   }



    }



    public function getAll($limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM docente
             ORDER BY iddocente
             LIMIT $limit
             OFFSET $offset";
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }
    }


    public function getByNameExceptId($name, $exceptId='')
    {
        $con = new Connection();
        $where = '';
        if( !empty($exceptId) )
            $where = " AND iddocente <> $exceptId ";
        $query =
            "SELECT *
             FROM docente
             WHERE nombre = '$name' 
             $where ";

$result = $con->execute_query($query);
if( pg_num_rows($result) > 0){    
    $combined=array();
    while ($row = pg_fetch_assoc($result)) {
        $combined[]=$row;
    }
   return $combined;
  }else{
      return null;
  }

    }



    public function search($search, $limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM docente
             WHERE nombre LIKE '%$search%' 

             ORDER BY nombre
             LIMIT $limit
             OFFSET $offset";
        
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }


    }



    public function save($nombre,$apellido,$ci,$sexo,$telefono,$correo,$direccion,$estado,$especialidad)
    {
        $con = new Connection();
        $query =
            "INSERT INTO docente (nombre, apellido,ci,sexo,telefono,correo,direccion,estado, tipo,especialidad) VALUES ('$nombre','$apellido',$ci,'$sexo','$telefono','$correo','$direccion','$estado','Docente','$especialidad') RETURNING iddocente;";
            $result = $con->getLastInsertedID($query);
            return $result;
    }



    public function update($id,$nombre,$apellido,$ci,$sexo,$telefono,$correo,$direccion,$estado,$especialidad)
    {
        $con = new Connection();
        $query =
            "UPDATE docente SET nombre  = '$nombre', apellido  = '$apellido', ci = $ci ,sexo='$sexo',telefono='$telefono', correo='$correo' ,direccion='$direccion', estado='$estado' ,especialidad='$especialidad'
             WHERE iddocente = $id";
        return $con->execute_query($query);
    }



    public function delete($id)
    {
        $con = new Connection();
        $query = "DELETE FROM docente WHERE iddocente = $id ";
        return $con->execute_query($query);
    }



}