<?php

class Oferta_Model
{
    public function getEmpty()
    {
        $oferta = array(
            'id_oferta' => '',
            'nroaula' => '',
            'hora' => '',
            'iddocente' => '',
            'idmateria' => '',
            'docenten' => '',
            'materian' => '',
            'curson' => ''
        );
        return $oferta;
    }



    public function getById($id)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM oferta
             WHERE id_oferta = $id ";
      $result = $con->execute_query($query);
      if( pg_num_rows($result) > 0){    
          $combined=array();
          while ($row = pg_fetch_assoc($result)) {
              $combined[]=$row;
          }
         return $combined;
        }else{
            return null;
        }

    }





    public function searchByName($name, $limit=9999, $offset=0)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM  oferta
             WHERE grado LIKE '%$name%'
             ORDER BY grado
             LIMIT $limit
             OFFSET $offset";
 $result = $con->execute_query($query);
 if( pg_num_rows($result) > 0){    
     $combined=array();
     while ($row = pg_fetch_assoc($result)) {
         $combined[]=$row;
     }
    return $combined;
   }else{
       return null;
   }



    }



    public function getAll($limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM oferta
             ORDER BY id_oferta asc
             LIMIT $limit
             OFFSET $offset";
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }
    }


    public function getByNameExceptId($name, $exceptId='')
    {
        $con = new Connection();
        $where = '';
        if( !empty($exceptId) )
            $where = " AND id_oferta <> $exceptId ";
        $query =
            "SELECT *
             FROM oferta
             WHERE grado = '$name' 
             $where ";

$result = $con->execute_query($query);
if( pg_num_rows($result) > 0){    
    $combined=array();
    while ($row = pg_fetch_assoc($result)) {
        $combined[]=$row;
    }
   return $combined;
  }else{
      return null;
  }

    }



    public function search($search, $limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM oferta
             WHERE docenten LIKE '%$search%' 
             ORDER BY docenten
             LIMIT $limit
             OFFSET $offset";
        
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }


    }



    public function save($nroaula,$hora,$iddocente,$idmateria,$docenten,$materian,$curson)
    {
        $con = new Connection();
        $query =
            "INSERT INTO oferta (nroaula,hora,iddocente,idmateria,docenten,materian,curson) VALUES ($nroaula,'$hora',$iddocente,$idmateria,'$docenten','$materian','$curson') RETURNING id_oferta;";
            $result = $con->getLastInsertedID($query);
            return $result;
    }



    public function update($id,$nroaula,$hora,$iddocente,$idmateria,$docenten,$materian,$curson)
    {
        $con = new Connection();
        $query =
            "UPDATE oferta
             SET nroaula  = $nroaula, hora = '$hora', iddocente=$iddocente,idmateria = $idmateria ,docenten='$docenten',materian='$materian',curson='$curson'
             WHERE id_oferta = $id";
        return $con->execute_query($query);
    }



    public function delete($id)
    {
        $con = new Connection();
        $query = "DELETE FROM oferta WHERE id_oferta = $id ";
        return $con->execute_query($query);
    }



}