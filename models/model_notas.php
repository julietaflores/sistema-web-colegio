<?php

class Notas_Model
{
    public function getEmpty()
    {
        $notas = array(
            'idnota' => '',
            'nota' => '',
            'bimestre' => '',
            'idinscripcion' => '',
            'alumnon' => '',
            'materian' => '',
            'curson' => ''
        );
        return $notas;
    }



    public function getById($id)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM nota
             WHERE idnota = $id ";
      $result = $con->execute_query($query);
      if( pg_num_rows($result) > 0){    
          $combined=array();
          while ($row = pg_fetch_assoc($result)) {
              $combined[]=$row;
          }
         return $combined;
        }else{
            return null;
        }

    }

    public function getByIda($id)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM nota n
             join inscripcion i on n.idinscripcion = i.idinscripcion  
             where i.idalumno=$id  ";
      $result = $con->execute_query($query);
      if( pg_num_rows($result) > 0){    
          $combined=array();
          while ($row = pg_fetch_assoc($result)) {
              $combined[]=$row;
          }
         return $combined;
        }else{
            return null;
        }

    }


    public function getAll($limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM nota
             ORDER BY idnota asc
             LIMIT $limit
             OFFSET $offset";
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }
    }


    public function getAllg($limit=9999, $offset=0)
    {
        $query =
            "SELECT alumnon
             FROM nota
             group BY alumnon 
             LIMIT $limit
             OFFSET $offset";
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }
    }

    public function search($search, $limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM nota
             WHERE alumnon LIKE '%$search%' 
             ORDER BY alumnon
             LIMIT $limit
             OFFSET $offset";
        
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }

    }

    public function searchgp($search, $limit=9999, $offset=0)
    {  $combined=array();
        $con = new Connection();
        $query =
            "SELECT alumnon
             FROM nota
             WHERE alumnon LIKE '%$search%'
             group BY alumnon  
             LIMIT $limit
             OFFSET $offset";
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){
            $combined[]="lista_notas";
           
            $query =
            "SELECT nombre
             FROM alumno
             WHERE nombre LIKE '%$search%'
             group BY nombre  
             LIMIT $limit
             OFFSET $offset";
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){
            $combined[]="lista_alumno";
        }


        return $combined;
          }else{
              return null;
          }

    }


    public function aprobados( $limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM nota
             WHERE nota >=51 
             ORDER BY alumnon
             LIMIT $limit
             OFFSET $offset";
        
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }


    }

    public function reprobados( $limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM nota
             WHERE nota <51 
             ORDER BY alumnon
             LIMIT $limit
             OFFSET $offset";
        
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }


    }


    public function save($nota,$bimestre,$idinscripcion,$alumnon,$materian,$curson)
    {
        $con = new Connection();
        $query =
            "INSERT INTO nota (nota,bimestre,idinscripcion,alumnon,materian,curson) VALUES ($nota,$bimestre,$idinscripcion,'$alumnon','$materian','$curson') RETURNING idnota;";
            $result = $con->getLastInsertedID($query);
            return $result;
    }



    public function update($id,$nota,$bimestre,$idinscripcion,$alumnon,$materian,$curson)
    {
        $con = new Connection();
        $query =
            "UPDATE nota
             SET nota = $nota, bimestre=$bimestre, idinscripcion=$idinscripcion,alumnon='$alumnon',materian='$materian' , curson='$curson'  WHERE idnota = $id";
        return $con->execute_query($query);
    }



    public function delete($id)
    {
        $con = new Connection();
        $query = "DELETE FROM nota WHERE idnota = $id ";
        return $con->execute_query($query);
    }



}