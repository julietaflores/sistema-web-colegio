<?php
class administrador_Model
{
    public function getEmpty()
    {
        $administrador = array(
            'id_admin' => '',
            'adm_login' => '',
            'adm_password' => ''
        );
        return $administrador;
    }

    //------------------------------------------------------------------
    public function getValidatedByLoginByPassword($login, $password)
    {
        if( empty($login) ){
            return null;
        }
           
        $con = new Connection();
        $encryptedPassword = $this->_getEncryptPassword($password);
        $query =
            "SELECT *
             FROM administrador
             WHERE adm_login = '$login' and  adm_password= '$encryptedPassword'";

        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;

        } else{
          
            $query =
            "SELECT *
             FROM alumno
             WHERE nombre = '$login' and  ci= '$password'";
             $result = $con->execute_query($query);
             if( pg_num_rows($result) > 0){
                $combined=array();
                while ($row = pg_fetch_assoc($result)) {
                    $combined[]=$row;
                }
               return $combined;
             }else{
                return null;
             }



         
         
         
           
        }
         
    }




    private function _getEncryptPassword($password)
    {
        return hash('sha256',  $password);
    }

    function updatePassword($idadministrador, $newPassword)
    {
        $con = new Connection();
        $encryptedPassword = $this->_getEncryptPassword($newPassword);
        $query =
            "UPDATE administrador
             SET adm_password = '$encryptedPassword'
            WHERE id_admin = $idadministrador";

        return $con->execute_query($query);
    }

 
    //------------------------------------------------------------------
    public function getById($id)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM administrador
             WHERE id_admin= $id
             ";

$result = $con->execute_query($query);
if( pg_num_rows($result) > 0){    
    $combined=array();
    while ($row = pg_fetch_assoc($result)) {
        $combined[]=$row;
    }
   return $combined;
  }else{
      return null;
  }
    }

    public function getByIdcont($id)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM contador
             WHERE id_contador= $id
             ";

$result = $con->execute_query($query);
if( pg_num_rows($result) > 0){    
    $combined=array();
    while ($row = pg_fetch_assoc($result)) {
        $combined[]=$row;
    }
   return $combined;
  }else{
      return null;
  }
    }


    public function getByIdcolor($id)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM color
             WHERE id_color= $id
             ";

$result = $con->execute_query($query);
if( pg_num_rows($result) > 0){    
    $combined=array();
    while ($row = pg_fetch_assoc($result)) {
        $combined[]=$row;
    }
   return $combined;
  }else{
      return null;
  }
    }


    public function getAll($limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM administrador
             LIMIT $limit
             OFFSET $offset";

        $con = new Connection();
        return pg_fetch_all($con->execute_query($query), MYSQLI_ASSOC);
    }

    public function search($search, $limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM administrador
             WHERE adm_login LIKE '%$search%'
             ORDER BY adm_login
             LIMIT $limit
             OFFSET $offset";

        $con = new Connection();
        return mysqli_fetch_all($con->execute_query($query), MYSQLI_ASSOC);
    }

    public function save($name, $contraseña)
    {
        $con = new Connection();
        $encryptedPassword = $this->_getEncryptPassword($contraseña);
        $query = "INSERT INTO administrador (adm_login, adm_password, adm_padre) VALUES ('$name', '$encryptedPassword', 0) RETURNING id_admin;";
        $result = $con->getLastInsertedID($query);
        return $result;
    }


    function updatePassword1($idadministrador, $newPassword)
    {
        $con = new Connection();
        $query =
        "SELECT *
         FROM administrador
         WHERE id_admin = $idadministrador
         ";

         $result = $con->execute_query($query);
         $insert_row = pg_fetch_row($result);
         $insert_id = $insert_row[2];

        $encryptedPassword = $this->_getEncryptPassword($newPassword,base64_decode($insert_id));

        $query =
            "UPDATE administrador
             SET adm_password = '$encryptedPassword'
            WHERE id_admin = $idadministrador";

      return $con->execute_query($query);
    }

    public function update($id, $name, $contraseña)
    { $con = new Connection();
   
               if($contraseña!=""){

               
                $encryptedPassword = $this->_getEncryptPassword($contraseña);
                $query = "UPDATE administrador SET adm_login = '$name' , adm_password='$encryptedPassword' WHERE id_admin = $id";
                return $con->execute_query($query);
               }else{
           
                $query = "UPDATE administrador SET adm_login = '$name'  WHERE id_admin = $id";
                return $con->execute_query($query);
               }
    }


    public function updatecont($id,$cont)
    { $con = new Connection();
                $query = "UPDATE contador SET cont=$cont  WHERE id_contador = $id";
                return $con->execute_query($query);
    }

    public function updatecolor($id,$color)
    { $con = new Connection();
                $query = "UPDATE color SET color='$color' WHERE id_color = $id";
                return $con->execute_query($query);
    }

    public function delete($id)
    {
        $query = "DELETE FROM administrador WHERE id_admin = $id ";
        $con = new Connection();
        return $con->execute_query($query);
    }
}