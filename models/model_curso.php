<?php

class Curso_Model
{
    public function getEmpty()
    {
        $curso = array(
            'id_curso' => '',
            'grado' => '',
            'cupo' => '',
            'idcurso' => '',
            'estado' => ''
        );
        return $curso;
    }



    public function getById($id)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM curso
             WHERE id_curso = $id ";
      $result = $con->execute_query($query);
      if( pg_num_rows($result) > 0){    
          $combined=array();
          while ($row = pg_fetch_assoc($result)) {
              $combined[]=$row;
          }
         return $combined;
        }else{
            return null;
        }

    }





    public function searchByName($name, $limit=9999, $offset=0)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM  curso
             WHERE grado LIKE '%$name%'
             ORDER BY grado
             LIMIT $limit
             OFFSET $offset";
 $result = $con->execute_query($query);
 if( pg_num_rows($result) > 0){    
     $combined=array();
     while ($row = pg_fetch_assoc($result)) {
         $combined[]=$row;
     }
    return $combined;
   }else{
       return null;
   }



    }



    public function getAll($limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM curso
             ORDER BY id_curso asc
             LIMIT $limit
             OFFSET $offset";
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }
    }


    public function getByNameExceptId($name, $exceptId='')
    {
        $con = new Connection();
        $where = '';
        if( !empty($exceptId) )
            $where = " AND id_curso <> $exceptId ";
        $query =
            "SELECT *
             FROM curso
             WHERE grado = '$name' 
             $where ";

$result = $con->execute_query($query);
if( pg_num_rows($result) > 0){    
    $combined=array();
    while ($row = pg_fetch_assoc($result)) {
        $combined[]=$row;
    }
   return $combined;
  }else{
      return null;
  }

    }



    public function search($search, $limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM curso
             WHERE grado LIKE '%$search%' 

             ORDER BY grado
             LIMIT $limit
             OFFSET $offset";
        
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }


    }



    public function save($grado,$cupo,$idnivel,$estado)
    {
        $con = new Connection();
        $query =
            "INSERT INTO curso (grado,cupo,idnivel,estado) VALUES ('$grado',$cupo,$idnivel,'$estado') RETURNING id_curso;";
            $result = $con->getLastInsertedID($query);
            return $result;
    }



    public function update($id,$grado,$cupo,$idnivel,$estado)
    {
        $con = new Connection();
        $query =
            "UPDATE curso
             SET grado  = '$grado', cupo  = $cupo, idnivel=$idnivel,estado = '$estado'
             WHERE id_curso = $id";
        return $con->execute_query($query);
    }



    public function delete($id)
    {
        $con = new Connection();
        $query = "DELETE FROM curso WHERE id_curso = $id ";
        return $con->execute_query($query);
    }



}