<?php

class Materia_Model
{
    public function getEmpty()
    {
        $materia = array(
            'idmateria' => '',
            'nombre' => '',
            'idmateria' => '',
            'estado' => ''
        );
        return $materia;
    }



    public function getById($id)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM materia
             WHERE idmateria = $id ";
      $result = $con->execute_query($query);
      if( pg_num_rows($result) > 0){    
          $combined=array();
          while ($row = pg_fetch_assoc($result)) {
              $combined[]=$row;
          }
         return $combined;
        }else{
            return null;
        }

    }





    public function searchByName($name, $limit=9999, $offset=0)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM  materia
             WHERE nombre LIKE '%$name%'
             ORDER BY nombre
             LIMIT $limit
             OFFSET $offset";
 $result = $con->execute_query($query);
 if( pg_num_rows($result) > 0){    
     $combined=array();
     while ($row = pg_fetch_assoc($result)) {
         $combined[]=$row;
     }
    return $combined;
   }else{
       return null;
   }



    }



    public function getAll($limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM materia
             ORDER BY idmateria asc
             LIMIT $limit
             OFFSET $offset";
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }
    }


    public function getByNameExceptId($name, $exceptId='')
    {
        $con = new Connection();
        $where = '';
        if( !empty($exceptId) )
            $where = " AND idmateria <> $exceptId ";
        $query =
            "SELECT *
             FROM materia
             WHERE nombre= '$name' 
             $where ";

$result = $con->execute_query($query);
if( pg_num_rows($result) > 0){    
    $combined=array();
    while ($row = pg_fetch_assoc($result)) {
        $combined[]=$row;
    }
   return $combined;
  }else{
      return null;
  }

    }



    public function search($search, $limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM materia
             WHERE nombre LIKE '%$search%' 

             ORDER BY nombre
             LIMIT $limit
             OFFSET $offset";
        
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }


    }



    public function save($nombre,$idcurso,$nombrem,$estado)
    {
        $con = new Connection();
        $query =
            "INSERT INTO materia (nombre,idcurso,nombrem,estado) VALUES ('$nombre',$idcurso,'$nombrem','$estado') RETURNING idmateria;";
            $result = $con->getLastInsertedID($query);
            return $result;
    }



    public function update($id,$nombre,$idcurso,$nombrem,$estado)
    {
        $con = new Connection();
        $query =
            "UPDATE materia
             SET nombre = '$nombre', idcurso  = $idcurso, nombrem='$nombrem', estado = '$estado'
             WHERE idmateria = $id";
        return $con->execute_query($query);
    }



    public function delete($id)
    {
        $con = new Connection();
        $query = "DELETE FROM materia WHERE idmateria = $id ";
        return $con->execute_query($query);
    }



}