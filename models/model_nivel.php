<?php

class Nivel_Model

{

    public function getEmpty()

    {

        $nivel = array(
            'id_nivel' => '',
            'nombre' => '',
            'descripcion' => '',
            'estado' => ''
        );
        return $nivel;

    }



    public function getById($id)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM nivel
             WHERE id_nivel = $id ";
      $result = $con->execute_query($query);
      if( pg_num_rows($result) > 0){    
          $combined=array();
          while ($row = pg_fetch_assoc($result)) {
              $combined[]=$row;
          }
         return $combined;
        }else{
            return null;
        }

    }





    public function searchByName($name, $limit=9999, $offset=0)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM nivel
             WHERE nombre LIKE '%$name%'
             ORDER BY nomnre
             LIMIT $limit
             OFFSET $offset";
 $result = $con->execute_query($query);
 if( pg_num_rows($result) > 0){    
     $combined=array();
     while ($row = pg_fetch_assoc($result)) {
         $combined[]=$row;
     }
    return $combined;
   }else{
       return null;
   }



    }



    public function getAll($limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM nivel
             ORDER BY id_nivel
             LIMIT $limit
             OFFSET $offset";
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }
    }


    public function getByNameExceptId($name, $exceptId='')
    {
        $con = new Connection();
        $where = '';
        if( !empty($exceptId) )
            $where = " AND id_nivel <> $exceptId ";
        $query =
            "SELECT *
             FROM nivel
             WHERE nombre = '$name' 
             $where ";

$result = $con->execute_query($query);
if( pg_num_rows($result) > 0){    
    $combined=array();
    while ($row = pg_fetch_assoc($result)) {
        $combined[]=$row;
    }
   return $combined;
  }else{
      return null;
  }

    }



    public function search($search, $limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM nivel
             WHERE nombre LIKE '%$search%' 

             ORDER BY nombre
             LIMIT $limit
             OFFSET $offset";
        
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }


    }



    public function save($name,$descripcion,$estado)
    {
        $con = new Connection();
        $query =
            "INSERT INTO nivel (nombre, descripcion,estado) VALUES ('$name','$descripcion','$estado') RETURNING id_nivel;";
            $result = $con->getLastInsertedID($query);
            return $result;
    }



    public function update($id, $name,$descripcion,$estado)
    {
        $con = new Connection();
        $query =
            "UPDATE nivel
             SET nombre  = '$name', descripcion  = '$descripcion', estado = '$estado'
             WHERE id_nivel = $id";
        return $con->execute_query($query);
    }



    public function delete($id)
    {
        $con = new Connection();
        $query = "DELETE FROM nivel WHERE id_nivel = $id ";
        return $con->execute_query($query);
    }



}