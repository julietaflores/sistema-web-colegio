<?php

class Alumno_Model

{

    public function getEmpty()

    {

        $alumno = array(
            'id_alumno' => '',
            'nombre' => '',
            'apellido' => '',
            'ci' => '',
            'fecha_nacimiento' => '',
            'sexo' => '',
            'direccion' => '',
            'estado' => ''
        );
        return $alumno;

    }



    public function getById($id)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM alumno
             WHERE id_alumno = $id ";
      $result = $con->execute_query($query);
      if( pg_num_rows($result) > 0){    
          $combined=array();
          while ($row = pg_fetch_assoc($result)) {
              $combined[]=$row;
          }
         return $combined;
        }else{
            return null;
        }

    }





    public function searchByName($name, $limit=9999, $offset=0)
    {
        $con = new Connection();
        $query =
            "SELECT *
             FROM alumno
             WHERE nombre LIKE '%$name%'
             ORDER BY nomnre
             LIMIT $limit
             OFFSET $offset";
 $result = $con->execute_query($query);
 if( pg_num_rows($result) > 0){    
     $combined=array();
     while ($row = pg_fetch_assoc($result)) {
         $combined[]=$row;
     }
    return $combined;
   }else{
       return null;
   }



    }



    public function getAll($limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM alumno
             ORDER BY id_alumno
             LIMIT $limit
             OFFSET $offset";
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }
    }


    public function getByNameExceptId($name, $exceptId='')
    {
        $con = new Connection();
        $where = '';
        if( !empty($exceptId) )
            $where = " AND id_alumno <> $exceptId ";
        $query =
            "SELECT *
             FROM alumno
             WHERE nombre = '$name' 
             $where ";

$result = $con->execute_query($query);
if( pg_num_rows($result) > 0){    
    $combined=array();
    while ($row = pg_fetch_assoc($result)) {
        $combined[]=$row;
    }
   return $combined;
  }else{
      return null;
  }

    }



    public function search($search, $limit=9999, $offset=0)
    {
        $query =
            "SELECT *
             FROM alumno
             WHERE nombre LIKE '%$search%' 

             ORDER BY nombre
             LIMIT $limit
             OFFSET $offset";
        
        $con = new Connection();
        $result = $con->execute_query($query);
        if( pg_num_rows($result) > 0){    
            $combined=array();
            while ($row = pg_fetch_assoc($result)) {
                $combined[]=$row;
            }
           return $combined;
          }else{
              return null;
          }


    }



    public function save($nombre,$apellido,$ci,$fecha_nacimiento,$sexo,$direccion,$estado)
    {
        $con = new Connection();
        $query =
            "INSERT INTO alumno (nombre, apellido,ci,fecha_nacimiento,sexo,direccion,estado, type_user) VALUES ('$nombre','$apellido',$ci,'$fecha_nacimiento','$sexo','$direccion','$estado','1') RETURNING id_alumno;";
            $result = $con->getLastInsertedID($query);
            return $result;
    }



    public function update($id,$nombre,$apellido,$ci,$fecha_nacimiento,$sexo,$direccion,$estado)
    {
        $con = new Connection();
        $query =
            "UPDATE alumno SET nombre  = '$nombre', apellido  = '$apellido', ci = $ci , fecha_nacimiento='$fecha_nacimiento',sexo='$sexo',direccion='$direccion', estado='$estado' 
             WHERE id_alumno = $id";
        return $con->execute_query($query);
    }



    public function delete($id)
    {
        $con = new Connection();
        $query = "DELETE FROM alumno WHERE id_alumno = $id ";
        return $con->execute_query($query);
    }



}