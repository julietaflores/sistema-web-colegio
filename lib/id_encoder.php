<?php
class IdEncoder
{
    public static function getEncoded($id)
    {
        return str_rot13(base64_encode(gzdeflate( $id )));
    }

    public static function getDecoded($codedId)
    {
        return @gzinflate(base64_decode(str_rot13( $codedId )));
    }
}