<?php
include_once('../configs.php');
include_once('../session_manager.php');
include('../connection.php');

include('../models/model_nivel.php');
include('../color.php');
$nivelModel = new Nivel_Model();


$cont = $userModel->getByIdcont(9);
if($cont){
    foreach ($cont AS $id => $info){
        $cont['id_contador']=$info['id_contador'];
        $cont['cont']=$info['cont'];
    }
    $con=$cont['cont']+1;
}
$userModel->updatecont(9, $con);



if (isset($_GET['id'])){
    $nivel = $nivelModel->getById($_GET['id']);
    if($nivel){
        foreach ($nivel AS $id => $info){
            $nivel['id_nivel']=$info['id_nivel'];
            $nivel['nombre']=$info['nombre'];
            $nivel['descripcion']=$info['descripcion'];
            $nivel['estado']=$info['estado'];
        }
    }
}else{
    $nivel = $nivelModel->getEmpty();
}
    

if (!isset($_POST['color'])){
if (count($_POST) > 0) {
        if (!isset($_GET['id'])) {
            $idNivel = $nivelModel->save( $_POST['nombre'], $_POST['descripcion'],$_POST['estado']);
        } else {
            if ($nivelModel->update( $_GET['id'], $_POST['nombre'], $_POST['descripcion'],$_POST['estado'] ))
                $idNivel = $_GET['id'];
        }

        if ($idNivel)
            header('Location: lista_nivel.php');
        else
            $message = 'Ocurrio un error al registrar el nivel.';
}
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo WEB_TITLE; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/bootstrap/css/bootstrap.min.css">
    <!-- JqueryUI -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/skins/skin-red.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/iCheck/flat/red.css">

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo LOCALHOST; ?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo LOCALHOST; ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo LOCALHOST; ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo LOCALHOST; ?>/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo LOCALHOST; ?>/dist/js/app.min.js"></script>

    
    <script>
        $(document).ready(function () {

            $('#add_team').click(function () {
                d1= $('#nombre').val();
                d2= $('#descripcion').val();
                d3= $('#estado').val();
                if((d1!="") && (d2!="") && (d3!="")  )  {
                    $("#add_team1").click();    
                }else{
                    alert("llenar campos vacios porfavor !!!");
                }
               
               
               
            });
        });

      
    </script>
</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

    <?php include("../include/header.php"); ?>
    <?php include("../include/sidebar.php"); ?>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
               Nivel
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-6">
                    <div class="box box-default">

                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-tag"></i> Nivel</h3>
                        </div>

                        <form method="post" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="nombre">NOMBRE(*)</label>
                                    <input class="form-control" name="nombre" id="nombre" type="text" value="<?php echo $nivel['nombre']; ?>" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="descripcion">DESCRIPCION(*)</label>
                                    <input class="form-control" name="descripcion" id="descripcion"  type="text" value="<?php echo $nivel['descripcion']; ?>" autofocus>
                                </div>

                            


                                <div class="form-group">
                                    <label for="estado">ESTADO(*)</label>
                                    <select class="form-control" name="estado" id="estado">
                                        <option value="">Elija una opción</option>
                                        <option value="Activo" <?php if($nivel['estado'] == '1') echo 'selected'?>>Activo</option>
                                        <option value="Inactivo" <?php if($nivel['estado'] == '0') echo 'selected'?>>Inactivo</option>
                                    </select>
                                </div>

                     
                            </div>

                            <button id="add_team1"  style="display: none;" ></button>
                        </form>

                        <div class="box-footer">
                                <button id="add_team" class="btn btn-primary" >GUARDAR</button>
                            </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
    <footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Tecno Web 2-2019.</b> 
    </div>
    <strong>Contador de Página: </strong> <?php echo $con?>
</footer>
</div>

</body>
</html>