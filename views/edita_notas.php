<?php
include_once('../configs.php');
include_once('../session_manager.php');
include('../connection.php');
include('../models/model_inscripcion.php');
include('../models/model_notas.php');
$inscripcionModel = new Inscripcion_Model();
$notasModel = new Notas_Model();
include('../color.php');



$cont = $userModel->getByIdcont(19);
if($cont){
    foreach ($cont AS $id => $info){
        $cont['id_contador']=$info['id_contador'];
        $cont['cont']=$info['cont'];
    }
    $con=$cont['cont']+1;
}
$userModel->updatecont(19, $con);

$inscripcionList = $inscripcionModel->getAllnn();

if (isset($_GET['id'])){
    $notas = $notasModel->getById($_GET['id']);
    if($notas){
        foreach ($notas AS $id => $info){
            $notas['idnota']=$info['idnota'];
            $notas['nota']=$info['nota'];
            $notas['bimestre']=$info['bimestre'];
            $notas['idinscripcion']=$info['idinscripcion'];
            $notas['alumnon']=$info['alumnon'];
            $notas['materian']=$info['materian'];
            $notas['curson']=$info['curson'];
        }
    }
}else{
    $notas = $notasModel->getEmpty();
}
    
if (!isset($_POST['color'])){
if (count($_POST) > 0) {
        if (!isset($_GET['id'])) {
            $idnotass = $notasModel->save( $_POST['nota'], $_POST['bimestre'],$_POST['idinscripcion'],$_POST['alumnon'],$_POST['materian'],$_POST['curson']);
        } else {
            if ($notasModel->update( $_GET['id'], $_POST['nota'], $_POST['bimestre'],$_POST['idinscripcion'],$_POST['alumnon'],$_POST['materian'] ,$_POST['curson']))
                $idnotass = $_GET['id'];
        }

        if ($idnotass){
            header('Location: lista_notas.php');
        }else{
            $message = 'Ocurrio un error al registrar la inscripcion.';
        }
}
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo WEB_TITLE; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/bootstrap/css/bootstrap.min.css">
    <!-- JqueryUI -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/skins/skin-red.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/iCheck/flat/red.css">

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo LOCALHOST; ?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo LOCALHOST; ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo LOCALHOST; ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo LOCALHOST; ?>/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo LOCALHOST; ?>/dist/js/app.min.js"></script>

    <script>
        $(document).ready(function () {

            $('#add_team').click(function () {
                d1= $('#nota').val();
                d2= $('#bimestre').val();
                d3= $('#idinscripcion').val();
                
                if((d1!="") && (d2!="") && (d3!="")   )  {
                    $("#add_team1").click();    
                }else{
                    alert("llenar campos vacios porfavor !!!");
                }  
            });
        });
    </script>
</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

    <?php include("../include/header.php"); ?>
    <?php include("../include/sidebar.php"); ?>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
               Notas
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-6">
                    <div class="box box-default">

                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-tag"></i> Notas</h3>
                        </div>

                        <form method="post" enctype="multipart/form-data">
                            <div class="box-body">
                              

                                <div class="form-group">
                                    <label for="nota">NOTA(*)</label>
                                    <input class="form-control" name="nota" id="nota" type="number" value="<?php echo $notas['nota']; ?>" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="bimestre">BIMESTRE(*)</label>
                                    <select class="form-control" name="bimestre" id="bimestre">
                                        <option value="">Elija un Bimestre</option>
                                        <option value="1" <?php if($notas['bimestre'] == '1') echo 'selected'?>>Bimestre 1</option>
                                        <option value="2" <?php if($notas['bimestre'] == '2') echo 'selected'?>>Bimestre 2</option>
                                        <option value="3" <?php if($notas['bimestre'] == '3') echo 'selected'?>>Bimestre 3</option>
                                        <option value="4" <?php if($notas['bimestre'] == '4') echo 'selected'?>>Bimestre 4</option>
                                    </select>
                                </div>

                        
                                <div class="form-group">
                                    <label for="idinscripcion">INSCRIPCION(*)</label>
                                    <select name="idinscripcion" id="idinscripcion" class="form-control">
                                        <option value="">Elija un Alumno</option>
                                        <?php foreach ($inscripcionList as $team) { ?>
                                            <option value="<?php echo $team['idinscripcion']; ?>" <?php if ($notas['idinscripcion'] == $team['idinscripcion']) echo 'selected' ?>><?php echo $team['alumnon']  ."    ". ' [ Esta en la Materia: ' . $team['materian'] .'  '. 'del curso: '.$team['curson'] . ']' ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                               
             
                                <input type="hidden" id="alumnon" name="alumnon" value="<?php echo $notas['alumnon']?>"/>
                                <input type="hidden" id="materian" name="materian" value="<?php echo $notas['materian']?>"/>
                                <input type="hidden" id="curson" name="curson" value="<?php echo $notas['curson']?>"/>

                          
                            </div>

                        
                            <button id="add_team1"  style="display: none;" ></button>
                        </form>
                        
                        <div class="box-footer">
                                <button id="add_team" class="btn btn-primary" >GUARDAR</button>
                            </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Tecno Web 2-2019.</b> 
    </div>
    <strong>Contador de Página: </strong> <?php echo $con?>
</footer>
</div>

</body>
</html>




<script type="text/javascript">	

  var select = document.getElementById('idinscripcion');
   select.addEventListener('change',
  function(){
    var selectedOption = this.options[select.selectedIndex];
                dd= selectedOption.value;
                $.ajax({
                        data: {
                            id: dd
                        },
                        url: '<?php echo LOCALHOST;?>/ajax/inscripciona.php',
                        type: 'post',
                        success: function (response) {
                            if (response !== ''){
                                document.getElementById("alumnon").value = response;
                            }
                        }
                    });

                    $.ajax({
                        data: {
                            id: dd
                        },
                        url: '<?php echo LOCALHOST;?>/ajax/inscripcionm.php',
                        type: 'post',
                        success: function (response) {
                            if (response !== ''){
                                document.getElementById("materian").value = response;
                            }
                        }
                    });

                    $.ajax({
                        data: {
                            id: dd
                        },
                        url: '<?php echo LOCALHOST;?>/ajax/inscripcionc.php',
                        type: 'post',
                        success: function (response) {
                            if (response !== ''){
                                document.getElementById("curson").value = response;
                            }
                        }
                    });
  });



</script>