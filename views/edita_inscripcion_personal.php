<?php
include_once('../configs.php');
include_once('../session_manager.php');
include('../connection.php');
include('../models/model_inscripcion.php');
include('../models/model_oferta.php');
include('../models/model_alumno.php');
$alumnoModel = new Alumno_Model();
$inscripcionModel = new Inscripcion_Model();
$ofertaModel = new Oferta_Model();
$alumnoList = $alumnoModel->getAll();
$ofertaList = $ofertaModel->getAll();


include('../color.php');
$cont = $userModel->getByIdcont(24);
if($cont){
    foreach ($cont AS $id => $info){
        $cont['id_contador']=$info['id_contador'];
        $cont['cont']=$info['cont'];
    }
    $con=$cont['cont']+1;
}
$userModel->updatecont(24, $con);


if (isset($_GET['id'])){
    $inscripcion = $inscripcionModel->getEmpty();
}
    



if (!isset($_POST['color'])){
   if (count($_POST) > 0) {
        if (isset($_GET['id'])) {
            $idinscripcionn = $inscripcionModel->save( $_POST['fecha'], $_POST['hora'],$_SESSION['id_user'],$_POST['idoferta'],$_POST['alumnon'],$_POST['materian'],$_POST['docenten'],$_POST['curson']);
        } 
        if ($idinscripcionn){
            header('Location: ainscripcion.php');
        }else{
            $message = 'Ocurrio un error al registrar la inscripcion.';
        }
   }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo WEB_TITLE; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/bootstrap/css/bootstrap.min.css">
    <!-- JqueryUI -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/skins/skin-red.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/iCheck/flat/red.css">

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo LOCALHOST; ?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo LOCALHOST; ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo LOCALHOST; ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo LOCALHOST; ?>/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo LOCALHOST; ?>/dist/js/app.min.js"></script>
    <script>
        $(document).ready(function () {

            $('#add_team').click(function () {
                d1= $('#fecha').val();
                d2= $('#hora').val();
                d3= $('#idoferta').val();
              
                if((d1!="") && (d2!="") && (d3!="")   )  {
                    $("#add_team1").click();    
                }else{
                    alert("llenar campos vacios porfavor !!!");
                }
               
               
               
            });
        });
    </script>
</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

    <?php include("../include/header.php"); ?>
    <?php include("../include/sidebar.php"); ?>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
               Inscripción
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-6">
                    <div class="box box-default">

                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-tag"></i> Inscripción</h3>
                        </div>

                        <form method="post" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="fecha">FECHA(*)</label>
                                    <input class="form-control" name="fecha" name="fecha" type="date" value="<?php echo $inscripcion['fecha']; ?>" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="hora">HORA(*)</label>
                                    <input class="form-control" name="hora" name="hora" type="time" value="<?php echo $inscripcion['hora']; ?>" autofocus>
                                </div>

                             
                               
                                <input type="hidden" id="alumnon" name="alumnon" value="<?php echo $inscripcion['alumnon']?>"/>


                           

                                <div class="form-group">
                                    <label for="idoferta">OFERTA(*)</label>
                                    <select name="idoferta" id="idoferta" class="form-control">
                                        <option value="">Elija una Oferta</option>
                                        <?php foreach ($ofertaList as $team) { ?>
                                            <option value="<?php echo $team['id_oferta']; ?>"><?php echo $team['docenten']  . ' [' ."curso: ".$team['curson'] ."   "."materia: ". $team['materian'] . ']'   ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                               
                                <input type="hidden" id="materian" name="materian" value="<?php echo $inscripcion['materian']?>"/>

                                <input type="hidden" id="docenten" name="docenten" value="<?php echo $inscripcion['docenten']?>"/>

                                <input type="hidden" id="curson" name="curson" value="<?php echo $inscripcion['curson']?>"/>
                           

                                <input type="hidden" id="ida" name="ida" value="<?php echo $_SESSION['id_user']?>"/>
                           
                    
                            </div>

                            <button id="add_team1"  style="display: none;" ></button>
                        </form>
                        
                        <div class="box-footer">
                                <button id="add_team" class="btn btn-primary" >GUARDAR</button>
                            </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
    <footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Tecno Web 2-2019.</b> 
    </div>
    <strong>Contador de Página: </strong> <?php echo $con?>
</footer>
</div>

</body>
</html>



<script type="text/javascript">	

  var select = document.getElementById('idoferta');
select.addEventListener('change',
  function(){
    var selectedOption = this.options[select.selectedIndex];
                dd= selectedOption.value;
                $.ajax({
                        data: {
                            id: dd
                        },
                        url: '<?php echo LOCALHOST;?>/ajax/nombremateriaof.php',
                        type: 'post',
                        success: function (response) {
                            if (response !== ''){
                                document.getElementById("materian").value = response;
                            }
                        }
                    });

                    $.ajax({
                        data: {
                            id: dd
                        },
                        url: '<?php echo LOCALHOST;?>/ajax/nombredocenteof.php',
                        type: 'post',
                        success: function (response) {
                            if (response !== ''){
                                document.getElementById("docenten").value = response;
                            }
                        }
                    });
                    $.ajax({
                        data: {
                            id: dd
                        },
                        url: '<?php echo LOCALHOST;?>/ajax/nombrecursoof.php',
                        type: 'post',
                        success: function (response) {
                            if (response !== ''){
                                document.getElementById("curson").value = response;
                            }
                        }
                    });



                    dd= $("#ida").val();
                
                $.ajax({
                        data: {
                            id: dd
                        },
                        url: '<?php echo LOCALHOST;?>/ajax/nombrealumno.php',
                        type: 'post',
                        success: function (response) {
                            if (response !== ''){
                                document.getElementById("alumnon").value = response;
                            }
                        }
                    });
  });



</script>