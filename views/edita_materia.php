<?php
include_once('../configs.php');
include_once('../session_manager.php');
include('../connection.php');
include('../models/model_curso.php');
include('../models/model_materia.php');

$materiaModel = new Materia_Model();
$cursoModel = new Curso_Model();

$cursoList = $cursoModel->getAll();

include('../color.php');



$cont = $userModel->getByIdcont(13);
if($cont){
    foreach ($cont AS $id => $info){
        $cont['id_contador']=$info['id_contador'];
        $cont['cont']=$info['cont'];
    }
    $con=$cont['cont']+1;
}
$userModel->updatecont(13, $con);



if (isset($_GET['id'])){
    $nivel = $materiaModel->getById($_GET['id']);
    if($nivel){
        foreach ($nivel AS $id => $info){
            $nivel['idmateria']=$info['idmateria'];
            $nivel['nombre']=$info['nombre'];
            $nivel['idcurso']=$info['idcurso'];
            $nivel['nombrem']=$info['nombrem'];
            $nivel['estado']=$info['estado'];
        }
    }
}else{
    $nivel = $materiaModel->getEmpty();
}
    

if (!isset($_POST['color'])){

    if (count($_POST) > 0) {
        if (!isset($_GET['id'])) {
            $nombrem = $materiaModel->save( $_POST['nombre'], $_POST['idcurso'],$_POST['nombrem'],$_POST['estado']);
        } else {
            if ($materiaModel->update( $_GET['id'], $_POST['nombre'], $_POST['idcurso'],$_POST['nombrem'],$_POST['estado'] ))
                $nombrem = $_GET['id'];
        }
        if ($nombrem){
            header('Location: lista_materia.php');
        }else{
            $message = 'Ocurrio un error al registrar la materia.';
        }
    }
  
}



?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo WEB_TITLE; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/bootstrap/css/bootstrap.min.css">
    <!-- JqueryUI -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/skins/skin-red.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/iCheck/flat/red.css">

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo LOCALHOST; ?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo LOCALHOST; ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo LOCALHOST; ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo LOCALHOST; ?>/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo LOCALHOST; ?>/dist/js/app.min.js"></script>
    <script>
        $(document).ready(function () {

            $('#add_team').click(function () {
                d1= $('#nombre').val();
                d2= $('#idcurso').val();
                d3= $('#estado').val();
                if((d1!="") && (d2!="") && (d3!="")  )  {
                    $("#add_team1").click();    
                }else{
                    alert("llenar campos vacios porfavor !!!");
                }
               
               
               
            });
        });

      
    </script>
</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

    <?php include("../include/header.php"); ?>
    <?php include("../include/sidebar.php"); ?>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
               Materia
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-6">
                    <div class="box box-default">

                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-tag"></i> Materia</h3>
                        </div>

                        <form method="post" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="nombre">NOMBRE(*)</label>
                                    <input class="form-control" name="nombre" id="nombre" type="text" value="<?php echo $nivel['nombre']; ?>" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="idcurso">CURSO(*)</label>
                                    <select name="idcurso" id="idcurso" class="form-control">
                                        <option value="">Elija una curso</option>
                                        <?php foreach ($cursoList as $team) { ?>
                                            <option value="<?php echo $team['id_curso']; ?>" <?php if ($nivel['idcurso'] == $team['id_curso']) echo 'selected' ?>><?php echo $team['grado']  ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                               
                                <input type="hidden" id="nombrem" name="nombrem" value="<?php echo $nivel['nombrem']?>"/>


                           


                                <div class="form-group">
                                    <label for="estado">ESTADO(*)</label>
                                    <select class="form-control" name="estado" id="estado">
                                        <option value="0">Elija una opción</option>
                                        <option value="1" <?php if($nivel['estado'] == '1') echo 'selected'?>>Activo</option>
                                        <option value="0" <?php if($nivel['estado'] == '0') echo 'selected'?>>Inactivo</option>
                                    </select>
                                </div>

                            </div>

                            <button id="add_team1"  style="display: none;" ></button>
                        </form>
                        
                        <div class="box-footer">
                                <button id="add_team" class="btn btn-primary" >GUARDAR</button>
                            </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Tecno Web 2-2019.</b> 
    </div>
    <strong>Contador de Página: </strong> <?php echo $con?>
</footer>
</div>

</body>
</html>


<script type="text/javascript">	



var select = document.getElementById('idcurso');
select.addEventListener('change',
  function(){
    var selectedOption = this.options[select.selectedIndex];
                dd= selectedOption.value;

                $.ajax({
                        data: {
                            id: dd
                        },
                        url: '<?php echo LOCALHOST;?>/ajax/nombrecurso.php',
                        type: 'post',
                        success: function (response) {
                            if (response !== ''){
                                document.getElementById("nombrem").value = response;
                            }
                        }
                    });
  });



</script>