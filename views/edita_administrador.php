<?php
include_once('../configs.php');
include_once('../session_manager.php');
include('../connection.php');
include('../models/model_user.php');

$administradorModel = new administrador_Model();



if (isset($_POST['color'])){
    $color=$_POST['color'];
    $administradorModel->updatecolor(1, $color);
}

$color = $administradorModel->getByIdcolor(1);

if($color){
    foreach ($color AS $id => $info){
        $color['id_color']=$info['id_color'];
        $color['color']=$info['color'];
    }
}





$cont = $administradorModel->getByIdcont(6);
if($cont){
    foreach ($cont AS $id => $info){
        $cont['id_contador']=$info['id_contador'];
        $cont['cont']=$info['cont'];
    }
    $con=$cont['cont']+1;
}
$administradorModel->updatecont(6, $con);

if (isset($_GET['id'])){
    $administrador = $administradorModel->getById($_GET['id']);
    if($administrador){
        foreach ($administrador AS $id => $info){
            $administrador['id_admin']=$info['id_admin'];
            $administrador['adm_login']=$info['adm_login'];
            
        }
    }
}else{
    $administrador = $administradorModel->getEmpty();
}
    

if (!isset($_POST['color'])){
  if (count($_POST) > 0) {
        if (!isset($_GET['id'])) {
            $idadministrador = $administradorModel->save( $_POST['adm_login'], $_POST['adm_password']);
        } else {
            if ($administradorModel->update( $_GET['id'], $_POST['adm_login'], $_POST['adm_password'] ))
                $idadministrador = $_GET['id'];
        }
        if ($idadministrador){
            header('Location: lista_administrador.php');
        }else{
            $message = 'Ocurrio un error al registrar el administrador.';
        } 
  }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo WEB_TITLE; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/bootstrap/css/bootstrap.min.css">
    <!-- JqueryUI -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/skins/skin-red.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/iCheck/flat/red.css">

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo LOCALHOST; ?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo LOCALHOST; ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo LOCALHOST; ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo LOCALHOST; ?>/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo LOCALHOST; ?>/dist/js/app.min.js"></script>


    <script>
        $(document).ready(function () {

            $('#add_team').click(function () {
                d1= $('#adm_login').val();
                d2= $('#adm_password').val();
                if((d1=="") && (d2=="")  )  {
                        alert("llenar campos !!!");
                }else{
                    if(d2==""){
                        alert("llenar campo contraseña !!!");
                    }else{
                        $("#add_team1").click();
                    }
                }
               
               
            });
        });

      
    </script>
</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

    <?php include("../include/header.php"); ?>
    <?php include("../include/sidebar.php"); ?>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
               administrador
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-6">
                    <div class="box box-default">

                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-tag"></i> administrador</h3>
                        </div>

                        <form method="post" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="adm_login">NOMBRE(*)</label>
                                    <input class="form-control" name="adm_login" id="adm_login" type="text" value="<?php echo $administrador['adm_login']; ?>" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="adm_password">PASSWORD(*)</label>
                                    <input class="form-control" name="adm_password" id="adm_password" type="text" value="<?php echo $administrador['adm_password']; ?>" autofocus>
                                </div>

                            
                            </div>

                           
                                <button id="add_team1"  style="display: none;" ></button>
                          

                            </form>
                            <div class="box-footer">
                                <button id="add_team" class="btn btn-primary" >GUARDAR</button>
                            </div>
                        

                    </div>
                </div>
            </div>
        </section>
    </div>

    
    <footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Tecno Web 2-2019.</b> 
    </div>
    <strong>Contador de Página: </strong> <?php echo $con?>
</footer>
</div>

</body>
</html>