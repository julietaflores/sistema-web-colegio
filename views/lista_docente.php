<?php
include_once('../configs.php');
include_once('../session_manager.php');
include('../connection.php');
include('../models/model_docente.php');

include('../models/model_user.php');
$userModel =  new administrador_Model();
$DocenteModel = new docente_Model();


if (isset($_POST['color'])){
    $color=$_POST['color'];
    $userModel->updatecolor(1, $color);
}

$color = $userModel->getByIdcolor(1);

if($color){
    foreach ($color AS $id => $info){
        $color['id_color']=$info['id_color'];
        $color['color']=$info['color'];
    }
}

$cont = $userModel->getByIdcont(4);
if($cont){
    foreach ($cont AS $id => $info){
        $cont['id_contador']=$info['id_contador'];
        $cont['cont']=$info['cont'];
    }
    $con=$cont['cont']+1;
}
$userModel->updatecont(4, $con);



if (isset($_POST['search'])){
    $docenteList = $DocenteModel->search($_POST['search']);
  
}else{
    $docenteList = $DocenteModel->getAll();

   
}
   
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo WEB_TITLE; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/bootstrap/css/bootstrap.min.css">
    <!-- jQuery UI 1.11.4 -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/skins/skin-red.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/iCheck/flat/red.css">

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo LOCALHOST; ?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo LOCALHOST; ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo LOCALHOST; ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo LOCALHOST; ?>/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo LOCALHOST; ?>/dist/js/app.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#search_nivel').click(function () {
                search_nivel();
            });

            $(document).on('keydown', '#search_text_nivel', function (e) {
                // Enter
                if (e.keyCode === 13)
                    search_nivel();
            });

            $('.delete_sport').click(function () {
                if (confirm('Desea eliminar este curso?')) {
                    $.ajax({
                        data: {
                            id: $(this).attr('curso_id')
                        },
                        url: '<?php echo LOCALHOST;?>/ajax/delete_docente.php',
                        type: 'post',
                        success: function (response) {
                            if (response !== ''){
                                alert(response);
                            }
                                location.reload();
                        }
                    });
                }
            });
        });

        function search_nivel() {
            $('#search_text_form').val($('#search_text_nivel').val());
            $('#search_page_form').val(1);
            $('#form_search').submit();
        }
    </script>
</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">
    <?php include("../include/header.php"); ?>
    <?php include("../include/sidebar.php"); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Lista de Docente
            </h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-tag"></i>Docente</h3>

                        <div class="pull-right box-tools">
                            <a href="<?php echo LOCALHOST; ?>/views/edita_docente.php" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> AGREGAR</a>
                        </div>
                    </div>
                    <div class="box-body table-responsive">

                        <div class="input-group">
                            <input id="search_text_nivel" placeholder="Buscador..." class="form-control" value="<?php if (isset($_POST['search'])) echo $_POST['search']; ?>">
                            <div class="input-group-btn">
                                <button id="search_nivel" class="btn btn-success">BUSCAR</button>
                            </div>
                        </div>

                             <?php if(empty($docenteList)){
                                  echo '<h3  style="   text-align: center;" >NO HAY LISTA DISPONIBLE EN ESTE MOMENTO !!!</h3>';
                                 ?>
                       

                             <?php }else{?> 

                                
                        <table class="table table-bordered table-condensed table-hover">
                            <tr>
                                <th>NOMBRE</th>
                                <th>APELLIDO</th>
                                <th>CI</th>
                                <th>SEXO</th>
                                <th>TELEFONO</th>
                                <th>CORREO</th>
                                <th>DIRECCION</th>
                                <th>ESPECIALIDAD</th>
                                <th>ESTADO</th>
                                <th></th>
                            </tr>

                                <?php
                            foreach ($docenteList as $nivel) {
                                ?>
                                <tr>
                                    <td><?php echo $nivel['nombre']; ?></td>
                                    <td><?php echo $nivel['apellido']; ?></td>
                                    
                                    <td><?php echo $nivel['ci']; ?></td>
                                    <td><?php echo $nivel['sexo']; ?></td>
                                    <td><?php echo $nivel['telefono']; ?></td>
                                    <td><?php echo $nivel['correo']; ?></td>
                                    <td><?php echo $nivel['direccion']; ?></td>
                                    <td><?php echo $nivel['especialidad']; ?></td>

                                    <?php  if($nivel['estado']=="1"){?>
                                       
                                        <td> <span class="text-fuchsia"><b>Activo</b></span></td>
                                         <?php }else{?>
                                            <td> <span class="text-fuchsia"><b>Inactivo</b></span> </td>
                                            <?php }?>
                                  
                                    <td align="center">
                                        <a href="<?php echo LOCALHOST; ?>/views/edita_docente.php?id=<?php echo $nivel['iddocente']; ?>" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                        <a class="delete_sport btn btn-xs btn-danger" curso_id="<?php echo $nivel['iddocente']; ?>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>

                                </table>

                                <?php }?> 
                        


                       
                    </div>
                </div>
            </div>
        </section>
    </div>
    <footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Tecno Web 2-2019.</b> 
    </div>
    <strong>Contador de Página: </strong> <?php echo $con?>
</footer>
</div>
</body>

</html>

<form id="form_search" method="post" style="display: none;">
    <input name="search" id="search_text_form">
    <input name="page" id="search_page_form">
</form>