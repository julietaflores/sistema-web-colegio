<?php
include_once('../configs.php');
include_once('../session_manager.php');
include('../connection.php');
include('../models/model_alumno.php');

$alumnoModel = new Alumno_Model();

include('../color.php');

$cont = $userModel->getByIdcont(7);
if($cont){
    foreach ($cont AS $id => $info){
        $cont['id_contador']=$info['id_contador'];
        $cont['cont']=$info['cont'];
    }
    $con=$cont['cont']+1;
}
$userModel->updatecont(7, $con);


if (isset($_GET['id'])){
    $alumno = $alumnoModel->getById($_GET['id']);
    if($alumno){
        foreach ($alumno AS $id => $info){
            $alumno['id_alumno']=$info['id_alumno'];
            $alumno['nombre']=$info['nombre'];
            $alumno['apellido']=$info['apellido'];
            $alumno['ci']=$info['ci'];
            $alumno['fecha_nacimiento']=$info['fecha_nacimiento'];
            $alumno['sexo']=$info['sexo'];
            $alumno['direccion']=$info['direccion'];
            $alumno['estado']=$info['estado'];
        }
    }
}else{
    $alumno = $alumnoModel->getEmpty();
}
    

if (!isset($_POST['color'])){
if (count($_POST) > 0) {
        if (!isset($_GET['id'])) {
            $idalumno = $alumnoModel->save( $_POST['nombre'], $_POST['apellido'],$_POST['ci'],$_POST['fecha_nacimiento'],$_POST['sexo'],$_POST['direccion'],$_POST['estado']);
        } else {
            if ($alumnoModel->update( $_GET['id'], $_POST['nombre'], $_POST['apellido'],$_POST['ci'],$_POST['fecha_nacimiento'],$_POST['sexo'],$_POST['direccion'],$_POST['estado'] ))
                $idalumno = $_GET['id'];
        }
        if ($idalumno){
            header('Location: lista_alumno.php');
        }else{
            $message = 'Ocurrio un error al registrar el alumno.';
        }
}
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo WEB_TITLE; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/bootstrap/css/bootstrap.min.css">
    <!-- JqueryUI -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/skins/skin-red.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/iCheck/flat/red.css">

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo LOCALHOST; ?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo LOCALHOST; ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo LOCALHOST; ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo LOCALHOST; ?>/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo LOCALHOST; ?>/dist/js/app.min.js"></script>


    <script>
        $(document).ready(function () {

            $('#add_team').click(function () {
                d1= $('#nombre').val();
                d2= $('#apellido').val();
                d3= $('#ci').val();
                d4= $('#fecha_nacimiento').val();
                d5= $('#sexo').val();
                d6= $('#direccion').val();
                d7= $('#estado').val();
                if((d1!="") && (d2!="") && (d3!="") && (d4!="") && (d5!="") && (d6!="") && (d7!="")   )  {
                    $("#add_team1").click();    
                }else{
                    alert("llenar campos vacios porfavor !!!");
                }
               
               
               
            });
        });

      
    </script>
</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

    <?php include("../include/header.php"); ?>
    <?php include("../include/sidebar.php"); ?>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
               Alumno
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-6">
                    <div class="box box-default">

                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-tag"></i> Alumno</h3>
                        </div>

                        <form method="post" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="nombre">NOMBRE(*)</label>
                                    <input class="form-control" name="nombre" id="nombre" type="text" value="<?php echo $alumno['nombre']; ?>" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="apellido">APELLIDO(*)</label>
                                    <input class="form-control" name="apellido" id="apellido" type="text" value="<?php echo $alumno['apellido']; ?>" autofocus>
                                </div>

                            
                                <div class="form-group">
                                    <label for="ci">CARNET DE IDENTIDAD(*)</label>
                                    <input class="form-control" name="ci" id="ci" type="number" value="<?php echo $alumno['ci']; ?>" autofocus>
                                </div>

                                
                                <div class="form-group">
                                    <label for="fecha_nacimiento">FECHA DE NACIMIENTO(*)</label>
                                    <input class="form-control" name="fecha_nacimiento" id="fecha_nacimiento" type="date" value="<?php echo $alumno['fecha_nacimiento']; ?>" autofocus>
                                </div>


                                <div class="form-group">
                                    <label for="sexo">SEXO(*)</label>
                                    <select class="form-control" name="sexo" id="sexo">
                                        <option value="">Elija una opción</option>
                                        <option value="Femenino" <?php if($alumno['sexo'] == 'Femenino') echo 'selected'?>>Femenino</option>
                                        <option value="Masculino" <?php if($alumno['sexo'] == 'Masculino') echo 'selected'?>>Masculino</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="direccion">DIRECCION(*)</label>
                                    <input class="form-control" name="direccion" id="direccion" type="text" value="<?php echo $alumno['direccion']; ?>" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="estado">ESTADO(*)</label>
                                    <select class="form-control" name="estado"  id="estado">
                                        <option value="">Elija una opción</option>
                                        <option value="1" <?php if($alumno['estado'] == '1') echo 'selected'?>>Activo</option>
                                        <option value="0" <?php if($alumno['estado'] == '0') echo 'selected'?>>Inactivo</option>
                                    </select>
                                </div>

                       
                            </div>

                            <button id="add_team1"  style="display: none;" ></button>
                          
                        </form>

                        <div class="box-footer">
                                <button id="add_team" class="btn btn-primary" >GUARDAR</button>
                            </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
    <footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Tecno Web 2-2019.</b> 
    </div>
    <strong>Contador de Página: </strong> <?php echo $con?>
</footer>
</div>

</body>
</html>