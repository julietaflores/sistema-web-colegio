<?php
include_once('../configs.php');
include_once('../session_manager.php');
include('../connection.php');
include('../models/model_oferta.php');
include('../models/model_docente.php');
include('../models/model_materia.php');
$materiaModel = new Materia_Model();
$docenteModel = new Docente_Model();
$ofertaModel = new Oferta_Model();
$docenteList = $docenteModel->getAll();

$materiaList = $materiaModel->getAll();

include('../color.php');



$cont = $userModel->getByIdcont(15);
if($cont){
    foreach ($cont AS $id => $info){
        $cont['id_contador']=$info['id_contador'];
        $cont['cont']=$info['cont'];
    }
    $con=$cont['cont']+1;
}
$userModel->updatecont(15, $con);

if (isset($_GET['id'])){
    $oferta = $ofertaModel->getById($_GET['id']);
    if($oferta){
        foreach ($oferta AS $id => $info){
            $oferta['id_oferta']=$info['id_oferta'];
            $oferta['nroaula']=$info['nroaula'];
            $oferta['hora']=$info['hora'];
            $oferta['iddocente']=$info['iddocente'];
            $oferta['idmateria']=$info['idmateria'];
            $oferta['docenten']=$info['docenten'];
            $oferta['materian']=$info['materian'];
            $oferta['curson']=$info['curson'];
        }
    }
}else{
    $oferta = $ofertaModel->getEmpty();
}
    

if (!isset($_POST['color'])){
if (count($_POST) > 0) {
        if (!isset($_GET['id'])) {
            $nroaulam = $ofertaModel->save( $_POST['nroaula'], $_POST['hora'],$_POST['iddocente'],$_POST['idmateria'],$_POST['docenten'],$_POST['materian'],$_POST['curson']);
        } else {
            if ($ofertaModel->update( $_GET['id'], $_POST['nroaula'], $_POST['hora'],$_POST['iddocente'],$_POST['idmateria'],$_POST['docenten'],$_POST['materian'],$_POST['curson'] ))
                $nroaulam = $_GET['id'];
        }
        if ($nroaulam){
            header('Location: lista_oferta.php');
        }else{
            $message = 'Ocurrio un error al registrar la oferta.';
        }
}
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo WEB_TITLE; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/bootstrap/css/bootstrap.min.css">
    <!-- JqueryUI -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/skins/skin-red.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/iCheck/flat/red.css">

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo LOCALHOST; ?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo LOCALHOST; ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo LOCALHOST; ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo LOCALHOST; ?>/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo LOCALHOST; ?>/dist/js/app.min.js"></script>

    <script>
        $(document).ready(function () {

            $('#add_team').click(function () {
                d1= $('#nroaula').val();
                d2= $('#hora').val();
                d3= $('#idcodente').val();
                d4= $('#idmateria').val();
                if((d1!="") && (d2!="") && (d3!="") && (d4!="")  )  {
                    $("#add_team1").click();    
                }else{
                    alert("llenar campos vacios porfavor !!!");
                }
               
               
               
            });
        });
    </script>
</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

    <?php include("../include/header.php"); ?>
    <?php include("../include/sidebar.php"); ?>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
               oferta
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-6">
                    <div class="box box-default">

                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-tag"></i> oferta</h3>
                        </div>

                        <form method="post" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="nroaula">NROAULA(*)</label>
                                    <input class="form-control" name="nroaula" id="nroaula" type="number" value="<?php echo $oferta['nroaula']; ?>" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="hora">HORA(*)</label>
                                    <input class="form-control" name="hora" id="hora" type="time" value="<?php echo $oferta['hora']; ?>" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="iddocente">DOCENTE(*)</label>
                                    <select name="iddocente" id="iddocente" class="form-control">
                                        <option value="">Elija una Docente</option>
                                        <?php foreach ($docenteList as $team) { ?>
                                            <option value="<?php echo $team['iddocente'];?>" <?php if ($oferta['iddocente'] == $team['iddocente']) echo 'selected' ?>><?php echo $team['nombre']  . ' [' . $team['especialidad'] . ']' ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                               
                                <input type="hidden" id="docenten" name="docenten" value="<?php echo $oferta['docenten']?>"/>


                           

                                <div class="form-group">
                                    <label for="idmateria">MATERIA(*)</label>
                                    <select name="idmateria" id="idmateria" class="form-control">
                                        <option value="">Elija una Materia</option>
                                        <?php foreach ($materiaList as $team) { ?>
                                            <option value="<?php echo $team['idmateria']; ?>" <?php if ($oferta['idmateria'] == $team['idmateria']) echo 'selected' ?>><?php echo $team['nombre']. ' [' ."curso: " . $team['nombrem'] . ']'   ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                               
                                <input type="hidden" id="materian" name="materian" value="<?php echo $oferta['materian']?>"/>


                                <input type="hidden" id="curson" name="curson" value="<?php echo $oferta['curson']?>"/>

                        
                            </div>

                            <button id="add_team1"  style="display: none;" ></button>
                        </form>
                        
                        <div class="box-footer">
                                <button id="add_team" class="btn btn-primary" >GUARDAR</button>
                            </div>


                    </div>
                </div>
            </div>
        </section>
    </div>

    <footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Tecno Web 2-2019.</b> 
    </div>
    <strong>Contador de Página: </strong> <?php echo $con?>
</footer>
</div>

</body>
</html>

<script type="text/javascript">	



var select1 = document.getElementById('iddocente');
select1.addEventListener('change', function(){
    var selectedOption = this.options[select1.selectedIndex];
                dd= selectedOption.value;
                
                $.ajax({
                        data: {
                            id: dd
                        },
                        url: '<?php echo LOCALHOST;?>/ajax/nombredocente.php',
                        type: 'post',
                        success: function (response) {
                            if (response !== ''){
                                document.getElementById("docenten").value = response;
                            }
                        }
                    });
  });





</script>


<script type="text/javascript">	

  var select = document.getElementById('idmateria');
select.addEventListener('change',
  function(){
    var selectedOption = this.options[select.selectedIndex];
                dd= selectedOption.value;
                $.ajax({
                        data: {
                            id: dd
                        },
                        url: '<?php echo LOCALHOST;?>/ajax/nombremateria.php',
                        type: 'post',
                        success: function (response) {
                            if (response !== ''){
                                document.getElementById("materian").value = response;
                            }
                        }
                    });

                    $.ajax({
                        data: {
                            id: dd
                        },
                        url: '<?php echo LOCALHOST;?>/ajax/nombrecursom.php',
                        type: 'post',
                        success: function (response) {
                            if (response !== ''){
                                document.getElementById("curson").value = response;
                            }
                        }
                    });
  });



</script>