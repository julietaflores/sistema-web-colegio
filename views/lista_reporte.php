<?php
include_once('../session_manager.php');
include('../connection.php');

include('fusioncharts.php');
include('../models/model_notas.php');
include('../models/model_inscripcion.php');

$notasModel = new Notas_Model();
$inscripcionModel = new Inscripcion_Model();


include('../color.php');

$inscripcionList = $inscripcionModel->getAll();
$cont = $userModel->getByIdcont(2);
if($cont){
    foreach ($cont AS $id => $info){
        $cont['id_contador']=$info['id_contador'];
        $cont['cont']=$info['cont'];
    }
    $con=$cont['cont']+1;
}


$userModel->updatecont(2, $con);

$notasList11 = $notasModel->getAllg(); 

if (isset($_POST['search'])){

    if($_POST['search']=='aprobado'){
        $notasList = $notasModel->aprobados();
    }else{
        if($_POST['search']=='reprobado'){
            $notasList = $notasModel->reprobados();
        }else{
            $notasList = $notasModel->search($_POST['search']);
        }
    }
  
 
}else{
    $notasList = $notasModel->getAll();  
}

?>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo WEB_TITLE; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/bootstrap/css/bootstrap.min.css">
    <!-- jQuery UI 1.11.4 -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/skins/skin-red.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/iCheck/flat/red.css">

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo LOCALHOST; ?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo LOCALHOST; ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo LOCALHOST; ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo LOCALHOST; ?>/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo LOCALHOST; ?>/dist/js/app.min.js"></script>
  
    <link rel="stylesheet" type="text/css" href="../FusionCharts/themes/fusioncharts.theme.fusion.css"></link>        
        <script type="text/javascript" src="//cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
        <script type="text/javascript" src="//cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
    
    <style>
th {
    text-align:left
}
        </style>

</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">
    <?php include("../include/header.php"); ?>
    <?php include("../include/sidebar.php"); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                REPORTE
            </h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="box box-default">
               
                    <div class="box-body table-responsive">


                    <div class="col-md-2">
                                    <label for="idinscripcion">Selecciona  un alumno</label>
                                    <select name="idinscripcion" id="idinscripcion" class="form-control">
                                        <option value="">Elija un Alumno</option>
                                        <?php foreach ($notasList11 as $team) { ?>
                                            <option value="<?php echo $team['alumnon']; ?>" ><?php echo $team['alumnon']?></option>
                                        <?php } ?>
                                        <option value="reprobado">Reprobados</option>
                                        <option value="aprobado">Aprobados</option>
                                    </select>
                                </div>
                               


                    <?php
    // Chart Configuration stored in Associative Array
    $arrChartConfig = array(
        "chart" => array(
            "caption" => "Reporte de Notas",
     //       "subCaption" => "In MMbbl = One Million barrels",
            "xAxisName" => "Country",
            "yAxisName" => "Maxima nota 100 puntos",
            "numberSuffix" => " Puntos",
            "theme" => "fusion"
        )
    );


    $arrChartConfigg = array(
        "chart" => array(
            "caption"=> "reporte torta",
            "showValues"=>"1",
            "showPercentInTooltip" =>"0",
            "numberPrefix" => "$",
            "enableMultiSlicing"=>"1",
            "theme"=>"fusion"
        )
    );
    // An array of hash objects which stores data

    $arrLabelValueData = array();

       foreach ($notasList as $nivel) {
           array_push($arrLabelValueData, array(
            "label" => $nivel['alumnon']." "."curso: ".$nivel['curson']."   "."materia: ".$nivel['materian'] , "value" => $nivel['nota']
            ));
       } 


    $arrChartConfig["data"] = $arrLabelValueData;
    $arrChartConfigg["data"] = $arrLabelValueData;
    // JSON Encode the data to retrieve the string containing the JSON representation of the data in the array.
    $jsonEncodedData = json_encode($arrChartConfig);
    $jsonEncodedDatag = json_encode($arrChartConfigg);


    
    // chart object
    $Chart = new FusionCharts("column2d", "MyFirstChart" , "700", "400", "chart-container", "json", $jsonEncodedData);
    // Render the chart
    $Chart->render();


    $Chart1 = new FusionCharts("pie3d", "chart-1" , "600", "400", "chart-container11", "json", $jsonEncodedDatag);
    // Render the chart
    $Chart1->render();

    ?>


<center>
        <div id="chart-container">Chart will render here!</div>
    </center>

     
   <!-- <center>
        <div id="chart-container11">Chart will render here!</div>
    </center>-->
                       
                    </div>
                </div>
            </div>
        </section>
    </div>

    <footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Tecno Web 2-2019.</b> 
    </div>
    <strong>Contador de Página: </strong> <?php echo $con?>
</footer>
</div>
</body>

</html>

<form id="form_search" method="post" style="display: none;">
    <input name="search" id="search_text_form">
</form>




<script type="text/javascript">	

  var select = document.getElementById('idinscripcion');
   select.addEventListener('change',
  function(){
    var selectedOption = this.options[select.selectedIndex];
                dd= selectedOption.value;
            $('#search_text_form').val(dd);
            $('#form_search').submit();
  });
</script>