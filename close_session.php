<?php
session_start();
unset($_SESSION['id_user']);
unset($_SESSION['name_user']);
unset($_SESSION['type_user']);

header('Location: login.php');